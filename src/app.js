const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
//const api = require('./Router/index');
const fronted = require('./Router/client');

//settings
app.set('views', path.join(__dirname,'views'))
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(morgan('dev'));
//engine
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
//router
//app.use('/api', api);
app.use('/', fronted);

//static file 
app.use(express.static(path.join(__dirname, 'views')));
app.use(cors());

module.exports = app;	