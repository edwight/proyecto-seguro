import { Component, OnInit } from '@angular/core';
import { SearchPipe } from '../../pipes/search.pipe';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-segurolist',
  templateUrl: './segurolist.component.html',
  styleUrls: ['./segurolist.component.css']
})
export class SegurolistComponent implements OnInit {
  lista:boolean = false;
  placa:string ='';
  data = [
    {
       id :1,
       name : "client A seguros ABC",
       industry: "Industry 1"

    },
    {
       id :2,
       name : "client Ab seguro RG",
       industry: "Industry 2"

    },
    {
       id :3,
       name : "client megha seguro vehicular",
       industry: "Industry 5"

    },
    {
       id :4,
       name : "shubham",
       industry: "Industry 1"

    },
    {
       id :4,
       name : "rick",
       industry: "Industry 1"

    }
 ];
  constructor() { }

  ngOnInit() {
  }
  mostarLista(){
  	//this.lista = true;
  	alert('ejemplo:'+this.placa);
  }
}
