import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SegurolistComponent } from './segurolist.component';

describe('SegurolistComponent', () => {
  let component: SegurolistComponent;
  let fixture: ComponentFixture<SegurolistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SegurolistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SegurolistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
