import { Component, OnInit,Input,Output } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

	@Input() item:any;
  constructor() { }

  ngOnInit() {
  }

}
