import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Route } from '@angular/router';
import {FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { PlacaComponent } from './components/placa/placa.component';
import { UserlistComponent } from './components/users/userlist/userlist.component';
import { UsershowComponent } from './components/users/usershow/usershow.component';
import { UsercreateComponent } from './components/users/usercreate/usercreate.component';
import { SegurolistComponent } from './components/segurolist/segurolist.component';
import { RegisterComponent } from './components/register/register.component';
import { SearchPipe } from './pipes/search.pipe';
import { LoginComponent } from './components/login/login.component';
import { ItemComponent } from './components/segurolist/item/item.component';
const routes:Route[] =[
  {path:'',component:SegurolistComponent},
  {path:'about',component:SegurolistComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
];
@NgModule({
  declarations: [
    AppComponent,
    PlacaComponent,
    SegurolistComponent,
    RegisterComponent,
    SearchPipe,
    LoginComponent,
    ItemComponent,
    //UserlistComponent,
    //UsershowComponent,
    //UsercreateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
