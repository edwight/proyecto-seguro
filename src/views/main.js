(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!--\n<app-register></app-register>\n-->\n<!--\n<app-segurolist></app-segurolist>\n-->\n\n<nav class=\"navbar navbar-default\">\n  <div class=\"container-fluid\">\n    <!-- Brand and toggle get grouped for better mobile display -->\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\n        <span class=\"sr-only\">Toggle navigation</span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n      </button>\n      <a class=\"navbar-brand\" href=\"#\">Brand</a>\n    </div>\n\n    <!-- Collect the nav links, forms, and other content for toggling -->\n    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n      <ul class=\"nav navbar-nav\">\n        <li class=\"active\"><a href=\"#\">Link <span class=\"sr-only\">(current)</span></a></li>\n        <li><a routerLink=\"/\">inicio</a></li>\n      </ul>\n      \n      <ul class=\"nav navbar-nav navbar-right\">\n        <li><a routerLink=\"/login\">Login</a></li>\n        <li class=\"dropdown\">\n          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Dropdown <span class=\"caret\"></span></a>\n          <ul class=\"dropdown-menu\">\n            <li><a href=\"#\">Action</a></li>\n            <li><a href=\"#\">Another action</a></li>\n            <li><a href=\"#\">Something else here</a></li>\n            <li role=\"separator\" class=\"divider\"></li>\n            <li><a href=\"#\">Separated link</a></li>\n          </ul>\n        </li>\n      </ul>\n    </div><!-- /.navbar-collapse -->\n  </div><!-- /.container-fluid -->\n</nav>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_placa_placa_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/placa/placa.component */ "./src/app/components/placa/placa.component.ts");
/* harmony import */ var _components_segurolist_segurolist_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/segurolist/segurolist.component */ "./src/app/components/segurolist/segurolist.component.ts");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/components/register/register.component.ts");
/* harmony import */ var _pipes_search_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pipes/search.pipe */ "./src/app/pipes/search.pipe.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_segurolist_item_item_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/segurolist/item/item.component */ "./src/app/components/segurolist/item/item.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    { path: '', component: _components_segurolist_segurolist_component__WEBPACK_IMPORTED_MODULE_6__["SegurolistComponent"] },
    { path: 'about', component: _components_segurolist_segurolist_component__WEBPACK_IMPORTED_MODULE_6__["SegurolistComponent"] },
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"] },
    { path: 'register', component: _components_register_register_component__WEBPACK_IMPORTED_MODULE_7__["RegisterComponent"] },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_placa_placa_component__WEBPACK_IMPORTED_MODULE_5__["PlacaComponent"],
                _components_segurolist_segurolist_component__WEBPACK_IMPORTED_MODULE_6__["SegurolistComponent"],
                _components_register_register_component__WEBPACK_IMPORTED_MODULE_7__["RegisterComponent"],
                _pipes_search_pipe__WEBPACK_IMPORTED_MODULE_8__["SearchPipe"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"],
                _components_segurolist_item_item_component__WEBPACK_IMPORTED_MODULE_10__["ItemComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n    <div id=\"login-overlay\" class=\"modal-dialog\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span><span class=\"sr-only\">Close</span></button>\n              <h4 class=\"modal-title\" id=\"myModalLabel\">Login to site.com</h4>\n          </div>\n          <div class=\"modal-body\">\n              <div class=\"row\">\n                  <div class=\"col-xs-6\">\n                      <div class=\"well\">\n                          <form id=\"loginForm\" method=\"POST\" action=\"/login/\" novalidate=\"novalidate\">\n                              <div class=\"form-group\">\n                                  <label for=\"username\" class=\"control-label\">Username</label>\n                                  <input type=\"text\" class=\"form-control\" id=\"username\" name=\"username\" value=\"\" required=\"\" title=\"Please enter you username\" placeholder=\"example@gmail.com\">\n                                  <span class=\"help-block\"></span>\n                              </div>\n                              <div class=\"form-group\">\n                                  <label for=\"password\" class=\"control-label\">Password</label>\n                                  <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" value=\"\" required=\"\" title=\"Please enter your password\">\n                                  <span class=\"help-block\"></span>\n                              </div>\n                              <div id=\"loginErrorMsg\" class=\"alert alert-error hide\">Wrong username og password</div>\n                              <div class=\"checkbox\">\n                                  <label>\n                                      <input type=\"checkbox\" name=\"remember\" id=\"remember\"> Remember login\n                                  </label>\n                                  <p class=\"help-block\">(if this is a private computer)</p>\n                              </div>\n                              <button type=\"submit\" class=\"btn btn-success btn-block\">Login</button>\n                              <a href=\"/forgot/\" class=\"btn btn-default btn-block\">Help to login</a>\n                          </form>\n                      </div>\n                  </div>\n                  <div class=\"col-xs-6\">\n                      <p class=\"lead\">Register now for <span class=\"text-success\">FREE</span></p>\n                      <ul class=\"list-unstyled\" style=\"line-height: 2\">\n                          <li><span class=\"fa fa-check text-success\"></span> See all your orders</li>\n                          <li><span class=\"fa fa-check text-success\"></span> Fast re-order</li>\n                          <li><span class=\"fa fa-check text-success\"></span> Save your favorites</li>\n                          <li><span class=\"fa fa-check text-success\"></span> Fast checkout</li>\n                          <li><span class=\"fa fa-check text-success\"></span> Get a gift <small>(only new customers)</small></li>\n                          <li><a href=\"/read-more/\"><u>Read more</u></a></li>\n                      </ul>\n                      <p><a href=\"/new-customer/\" class=\"btn btn-info btn-block\">Yes please, register now!</a></p>\n                  </div>\n              </div>\n          </div>\n      </div>\n  </div>"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoginComponent = /** @class */ (function () {
    function LoginComponent() {
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/placa/placa.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/placa/placa.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.container {\r\n  background: #F8A434;\r\n  font-family: 'Lato', sans-serif;\r\n  color: #FDFCFB;\r\n  text-align: center;\r\n  min-height: -moz-available;\r\n  min-height: fill-available;\r\n  min-height: -webkit-fill-available;\r\n  min-height: -moz-fill-available;\r\n}\r\n\r\n\r\nform {\r\n  width: 450px;\r\n  margin: 17% auto;\r\n}\r\n\r\n\r\n.header {\r\n  font-size: 35px;\r\n  text-transform: uppercase;\r\n  letter-spacing: 5px;\r\n}\r\n\r\n\r\n.description {\r\n  font-size: 14px;\r\n  letter-spacing: 1px;\r\n  line-height: 1.3em;\r\n  margin: -2px 0 45px;\r\n  display: flex;\r\n}\r\n\r\n\r\n.input {\r\n  display: flex;\r\n  align-items: center;\r\n}\r\n\r\n\r\n.button {\r\n  height: 44px;\r\n  border: none;\r\n}\r\n\r\n\r\n#placa {\r\n  /*width: 75%;*/\r\n  /*background: #FDFCFB;*/\r\n  font-family: inherit;\r\n  color: #737373;\r\n  letter-spacing: 1px;\r\n  text-indent: 5%;\r\n  /*border-radius: 5px 0 0 5px;*/\r\n}\r\n\r\n\r\n.btn-buscar {\r\n  width: 25%;\r\n  height: 46px;\r\n  background: rgb(0, 127, 173);\r\n  font-family: inherit;\r\n  font-weight: bold;\r\n  color: inherit;\r\n  letter-spacing: 1px;\r\n  border-radius: 0 5px 5px 0;\r\n  cursor: pointer;\r\n  transition: background .3s ease-in-out;\r\n}\r\n\r\n\r\n.btn-buscar:hover {\r\n  background: #d45d7d;\r\n}\r\n\r\n\r\ninput:focus {\r\n  outline: none;\r\n  outline: 2px solid #E86C8D;\r\n  box-shadow: 0 0 2px #E86C8D;\r\n}\r\n\r\n\r\n.formulario {\r\n  color: #212121;\r\n  text-align: center;\r\n}\r\n\r\n\r\n.formulario input[type=text] {\r\n  margin-bottom: 20px;\r\n  padding: 10px;\r\n  width: 100%;\r\n}\r\n\r\n\r\n.formulario input[type=text].error {\r\n  border: 5px solid #D32F2F;\r\n  color: red;\r\n}\r\n\r\n\r\n.formulario .boton {\r\n  background: none;\r\n  border: 1px solid #D32F2F;\r\n  color: #fff;\r\n  display: inline-block;\r\n  font-size: 16px;\r\n  padding: 10px 15px;\r\n}\r\n\r\n\r\n/*\r\n.principal .formulario .boton:hover {\r\n  border: 1px solid #fff;\r\n}\r\n*/\r\n\r\n\r\n#center-element{\r\n  text-align: center !important;\r\n  display: flex;\r\n  align-items:flex-start;\r\n}\r\n"

/***/ }),

/***/ "./src/app/components/placa/placa.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/placa/placa.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n      <div class=\"row\">\n        <div class=\"header\">\n            <p>Seguros</p>\n        </div>\n        <div class=\"col-sm-4\"> \n          <div class=\"description\">\n            <figure>\n              <img width=\"150\" alt=\"Angular Logo\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg==\">\n            </figure>\n          </div>\n        </div>\n        <div class=\"col-sm-8\">\n            <p>Encuentra el seguro que se adapte a ti </p>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"wrap\">\n          <form class=\"formulario\" action=\"\">\n            <div id=\"center-element\">\n              <input type=\"text\" id=\"placa\" placeholder=\"Placa del vehículo ejem abc123\">\n              <input type=\"button\" class=\"boton\" id=\"btn-buscar\" value=\"Buscar\">\n            </div>\n            <p>\n              <a href=\"#\">no tengo placa todavia</a>\n            </p>\n            \n          </form>\n        </div>\n      </div>       \n\n</div>\n"

/***/ }),

/***/ "./src/app/components/placa/placa.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/placa/placa.component.ts ***!
  \*****************************************************/
/*! exports provided: PlacaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlacaComponent", function() { return PlacaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PlacaComponent = /** @class */ (function () {
    function PlacaComponent() {
    }
    PlacaComponent.prototype.ngOnInit = function () {
    };
    PlacaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-placa',
            template: __webpack_require__(/*! ./placa.component.html */ "./src/app/components/placa/placa.component.html"),
            styles: [__webpack_require__(/*! ./placa.component.css */ "./src/app/components/placa/placa.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PlacaComponent);
    return PlacaComponent;
}());



/***/ }),

/***/ "./src/app/components/register/register.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/register/register.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "strong { font-weight: 500; }\r\n\r\na, a:hover, a:focus {\r\n\tcolor: #f35b3f;\r\n\ttext-decoration: none; transition: all .3s;\r\n}\r\n\r\nh1, h2 {\r\n\tmargin-top: 10px;\r\n\tfont-size: 38px;\r\n    font-weight: 100;\r\n    color: #555;\r\n    line-height: 50px;\r\n}\r\n\r\nh3 {\r\n\tfont-size: 22px;\r\n    font-weight: 300;\r\n    color: #555;\r\n    line-height: 30px;\r\n}\r\n\r\nh4 {\r\n\tfont-size: 18px;\r\n    font-weight: 300;\r\n    color: #555;\r\n    line-height: 26px;\r\n}\r\n\r\nimg { max-width: 100%; }\r\n\r\n::-moz-selection { background: #f35b3f; color: #fff; text-shadow: none; }\r\n\r\n::selection { background: #f35b3f; color: #fff; text-shadow: none; }\r\n\r\n/***** Top menu *****/\r\n\r\n.navbar {\r\n\tpadding-top: 10px;\r\n\tbackground: #333;\r\n\tbackground: rgba(51, 51, 51, 0.3);\r\n\tborder: 0; transition: all .3s;\r\n}\r\n\r\n.navbar.navbar-no-bg { background: none; }\r\n\r\nul.navbar-nav {\r\n\tfont-size: 16px;\r\n\tcolor: #fff;\r\n}\r\n\r\n.navbar-inverse ul.navbar-nav li { padding-top: 8px; padding-bottom: 8px; }\r\n\r\n.navbar-inverse ul.navbar-nav li .li-text { opacity: 0.8; }\r\n\r\n.navbar-inverse ul.navbar-nav li a { display: inline; padding: 0; color: #fff; }\r\n\r\n.navbar-inverse ul.navbar-nav li a:hover { color: #fff; opacity: 1; border-bottom: 1px dotted #fff; }\r\n\r\n.navbar-inverse ul.navbar-nav li a:focus { color: #fff; outline: 0; opacity: 1; border-bottom: 1px dotted #fff; }\r\n\r\n.navbar-inverse ul.navbar-nav li .li-social a {\r\n\tmargin: 0 5px;\r\n\tfont-size: 28px;\r\n\tvertical-align: middle;\r\n}\r\n\r\n.navbar-inverse ul.navbar-nav li .li-social a:hover, \r\n.navbar-inverse ul.navbar-nav li .li-social a:focus { border: 0; color: #f35b3f; }\r\n\r\n.navbar-brand {\r\n\twidth: 162px;\r\n\t/*background: url(../img/logo.png) left center no-repeat;*/\r\n\ttext-indent: -99999px;\r\n}\r\n\r\n/***** Top content *****/\r\n\r\n.top-content { padding: 40px 0 170px 0; }\r\n\r\n.top-content .text { color: #fff; }\r\n\r\n.top-content .text h1 { color: #fff; }\r\n\r\n.top-content .description { margin: 20px 0 10px 0; }\r\n\r\n.top-content .description p { opacity: 0.8; }\r\n\r\n.top-content .description a { color: #fff; }\r\n\r\n.top-content .description a:hover, \r\n.top-content .description a:focus { border-bottom: 1px dotted #fff; }\r\n\r\n.form-box { padding-top: 40px; }\r\n\r\n.f1 {\r\n\tpadding: 25px; background: #fff; border-radius: 4px;\r\n}\r\n\r\n.f1 h3 { margin-top: 0; margin-bottom: 5px; text-transform: uppercase; }\r\n\r\n.f1-steps { overflow: hidden; position: relative; margin-top: 20px; }\r\n\r\n.f1-progress { position: absolute; top: 24px; left: 0; width: 100%; height: 1px; background: #ddd; }\r\n\r\n.f1-progress-line { position: absolute; top: 0; left: 0; height: 1px; background: #f35b3f; }\r\n\r\n.f1-step { position: relative; float: left; width: 33.333333%; padding: 0 5px; }\r\n\r\n.f1-step-icon {\r\n\tdisplay: inline-block; width: 40px; height: 40px; margin-top: 4px; background: #ddd;\r\n\tfont-size: 16px; color: #fff; line-height: 40px; border-radius: 50%;\r\n}\r\n\r\n.f1-step.activated .f1-step-icon {\r\n\tbackground: #fff; border: 1px solid #f35b3f; color: #f35b3f; line-height: 38px;\r\n}\r\n\r\n.f1-step.active .f1-step-icon {\r\n\twidth: 48px; height: 48px; margin-top: 0; background: #f35b3f; font-size: 22px; line-height: 48px;\r\n}\r\n\r\n.f1-step p { color: #ccc; }\r\n\r\n.f1-step.activated p { color: #f35b3f; }\r\n\r\n.f1-step.active p { color: #f35b3f; }\r\n\r\n.f1 fieldset { display: none; text-align: left; }\r\n\r\n.f1-buttons { text-align: right; }\r\n\r\n.f1 .input-error { border-color: #f35b3f; }\r\n\r\n/***** Media queries *****/\r\n\r\n@media (min-width: 992px) and (max-width: 1199px) {}\r\n\r\n@media (min-width: 768px) and (max-width: 991px) {}\r\n\r\n@media (max-width: 767px) {\r\n\t\r\n\t.navbar { padding-top: 0; }\r\n\t.navbar.navbar-no-bg { background: #333; background: rgba(51, 51, 51, 0.9); }\r\n\t.navbar-brand { height: 60px; margin-left: 15px; }\r\n\t.navbar-collapse { border: 0; }\r\n\t.navbar-toggle { margin-top: 12px; }\r\n\t\r\n\t.top-content { padding: 40px 0 110px 0; }\r\n\r\n}\r\n\r\n@media (max-width: 415px) {\r\n\t\r\n\th1, h2 { font-size: 32px; }\r\n\t\r\n\t.f1 { padding-bottom: 20px; }\r\n\t.f1-buttons button { margin-bottom: 5px; }\r\n\r\n}\r\n\r\n/* Retina-ize images/icons */\r\n\r\n@media\r\nonly screen and (-webkit-min-device-pixel-ratio: 2), only screen and (   min--moz-device-pixel-ratio: 2), only screen and (        min-device-pixel-ratio: 2), only screen and (                -webkit-min-device-pixel-ratio: 2), only screen and (                min-resolution: 192dpi), only screen and (                min-resolution: 2dppx) {\r\n\t\r\n\t/* logo */\r\n    .navbar-brand {\r\n    \t/*background-image: url(../img/logo@2x.png) !important; */\r\n    \tbackground-repeat: no-repeat !important; \r\n    \tbackground-size: 162px 36px !important;\r\n    }\r\n\t\r\n}"

/***/ }),

/***/ "./src/app/components/register/register.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/register/register.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\n<nav class=\"navbar navbar-inverse navbar-no-bg\" role=\"navigation\">\n\t<div class=\"container\">\n\t\t<div class=\"navbar-header\">\n\t\t\t<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#top-navbar-1\">\n\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>\n\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t</button>\n\t\t\t<a class=\"navbar-brand\" href=\"index.html\">BootZard - Bootstrap Wizard Template</a>\n\t\t</div>\n\n\t\t<div class=\"collapse navbar-collapse\" id=\"top-navbar-1\">\n\t\t\t<ul class=\"nav navbar-nav navbar-right\">\n\t\t\t\t<li>\n\t\t\t\t\t<span class=\"li-text\">\n\t\t\t\t\t\tPut some text or\n\t\t\t\t\t</span> \n\t\t\t\t\t<a href=\"#\"><strong>links</strong></a> \n\t\t\t\t\t<span class=\"li-text\">\n\t\t\t\t\t\there, or some icons: \n\t\t\t\t\t</span> \n\t\t\t\t\t<span class=\"li-social\">\n\t\t\t\t\t\t<a href=\"https://www.facebook.com/pages/Azmindcom/196582707093191\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a> \n\t\t\t\t\t\t<a href=\"https://twitter.com/anli_zaimi\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a> \n\t\t\t\t\t\t<a href=\"https://plus.google.com/+AnliZaimi_azmind\" target=\"_blank\"><i class=\"fa fa-google-plus\"></i></a> \n\t\t\t\t\t\t<a href=\"https://github.com/AZMIND\" target=\"_blank\"><i class=\"fa fa-github\"></i></a>\n\t\t\t\t\t</span>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</div>\n\t</div>\n</nav>\n-->\n\n<!-- Top content -->\n<div class=\"top-content\">\n  <div class=\"container\">\n        \n    <div class=\"row\">\n        <div class=\"col-sm-8 col-sm-offset-2 text\">\n            <h1>Free <strong>Bootstrap</strong> Wizard</h1>\n            <div class=\"description\">\n           \t    <p>\n                    This is a free responsive Bootstrap form wizard. \n                    Download it on <a href=\"http://azmind.com\"><strong>AZMIND</strong></a>, customize and use it as you like!\n                </p>\n            </div>\n        </div>\n    </div>\n        \n    <div class=\"row\">\n      <div class=\"col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box\">\n      \t<form role=\"form\" action=\"api/register\" method=\"post\" class=\"f1\">\n\n      \t\t<h3>Register To Our App</h3>\n      \t\t<p>Fill in the form to get instant access</p>\n      \t\t<div class=\"f1-steps\">\n      \t\t\t<div class=\"f1-progress\">\n      \t\t\t  <div class=\"f1-progress-line\" data-now-value=\"16.66\" data-number-of-steps=\"3\" style=\"width: 16.66%;\"></div>\n      \t\t\t</div>\n      \t\t\t<div class=\"f1-step active\">\n      \t\t\t\t<div class=\"f1-step-icon\"><i class=\"fa fa-user\"></i></div>\n      \t\t\t\t<p>about</p>\n      \t\t\t</div>\n      \t\t\t<div class=\"f1-step\">\n      \t\t\t\t<div class=\"f1-step-icon\"><i class=\"fa fa-key\"></i></div>\n      \t\t\t\t<p>account</p>\n      \t\t\t</div>\n      \t\t    <div class=\"f1-step\">\n      \t\t\t\t<div class=\"f1-step-icon\"><i class=\"fa fa-twitter\"></i></div>\n      \t\t\t\t<p>social</p>\n      \t\t\t</div>\n      \t\t</div>\n      \t\t\n      \t\t<fieldset>\n      \t\t    <h4>Que tipo de seguro esta buscando:</h4>\n      \t\t\t<div class=\"form-group\">\n      \t\t\t    <label class=\"sr-only\" for=\"f1-first-name\">Soap</label>\n                      <input type=\"text\" name=\"soap\" placeholder=\"soap (seguro obligatorio)\" class=\"f1-first-name form-control\" id=\"f1-first-name\">\n                  </div>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-last-name\">particular</label>\n                      <input type=\"text\" name=\"particular\" placeholder=\"seguro particular\" class=\"f1-last-name form-control\" id=\"f1-last-name\">\n                  </div>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-about-yourself\">About yourself</label>\n                      <textarea name=\"f1-about-yourself\" placeholder=\"About yourself...\" class=\"f1-about-yourself form-control\" id=\"f1-about-yourself\"></textarea>\n                  </div>\n                  <div class=\"f1-buttons\">\n                      <button type=\"button\" class=\"btn btn-next\">Next</button>\n                  </div>\n              </fieldset>\n\n              <fieldset>\n                  <h4>Set up your account:</h4>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-email\">clase de Vehiculo</label>\n                      <input type=\"text\" name=\"clase-vehiculo\" placeholder=\"Clase de Vehiculo\" class=\"f1-email form-control\" id=\"f1-email\">\n                  </div>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-password\">Modelo</label>\n                      <input type=\"text\" name=\"modelo\" placeholder=\"Modelo\" class=\"f1-password form-control\" id=\"f1-password\">\n                  </div>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-repeat-password\">Ciudad de Circulacion</label>\n                      <input type=\"text\" name=\"ciudad\" placeholder=\"Ciudad de Circulacion\" \n                      class=\"f1-repeat-password form-control\" id=\"f1-repeat-password\">\n                  </div>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-repeat-password\">Marca del Vehiculo</label>\n                      <input type=\"text\" name=\"marca\" placeholder=\"Marca del Vehiculo\" \n                      class=\"f1-repeat-password form-control\" id=\"f1-repeat-password\">\n                  </div>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-repeat-password\">Financiado</label>\n                      <input type=\"text\" name=\"financiado\" placeholder=\"Financiado\" \n                      class=\"f1-repeat-password form-control\" id=\"f1-repeat-password\">\n                  </div>\n                  <div class=\"f1-buttons\">\n                      <button type=\"button\" class=\"btn btn-previous\">Previous</button>\n                      <button type=\"button\" class=\"btn btn-next\">Next</button>\n                  </div>\n              </fieldset>\n\n              <fieldset>\n                  <h4>Social media profiles:</h4>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-facebook\">Tipo de Documento</label>\n                      <input type=\"text\" name=\"tipo-documento\" placeholder=\"Tipo de Documento\" class=\"f1-facebook form-control\" id=\"f1-facebook\">\n                  </div>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-twitter\">Numero de Documento</label>\n                      <input type=\"text\" name=\"n-documento\" placeholder=\"Numero de Documento\" class=\"f1-twitter form-control\" id=\"f1-twitter\">\n                  </div>\n                  <div class=\"form-group\">\n                      <label class=\"sr-only\" for=\"f1-google-plus\">nombre</label>\n                      <input type=\"text\" name=\"nombre\" placeholder=\"nombre\" class=\"f1-google-plus form-control\" id=\"f1-google-plus\">\n                  </div>\n                  <div class=\"f1-buttons\">\n                      <button type=\"button\" class=\"btn btn-previous\">Previous</button>\n                      <button type=\"submit\" class=\"btn btn-submit\">Submit</button>\n                  </div>\n              </fieldset>\n      \t\n      \t</form>\n      </div>\n    </div>\n            \n  </div>\n</div>"

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/register/register.component.ts ***!
  \***********************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RegisterComponent = /** @class */ (function () {
    function RegisterComponent() {
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/components/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/components/segurolist/item/item.component.css":
/*!***************************************************************!*\
  !*** ./src/app/components/segurolist/item/item.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* items list */\r\n.item{margin-bottom: 12px;}\r\n.glyphicon { margin-right:5px;}\r\n.section-box h2 { margin-top:0px;}\r\n.section-box h2 a { font-size:15px; }\r\n.glyphicon-heart { color:#e74c3c;}\r\n.glyphicon-comment { color:#27ae60;}\r\n.separator { padding-right:5px;padding-left:5px; }\r\n.section-box hr {margin-top: 0;margin-bottom: 5px;border: 0;border-top: 1px solid rgb(199, 199, 199);\r\n}"

/***/ }),

/***/ "./src/app/components/segurolist/item/item.component.html":
/*!****************************************************************!*\
  !*** ./src/app/components/segurolist/item/item.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-6 item\">\n    <div class=\"well well-sm\">\n      <div class=\"row\">\n        <div class=\"col-xs-3 col-md-3 text-center\">\n          <img src=\"http://bootsnipp.com/apple-touch-icon-114x114-precomposed.png\" alt=\"bootsnipp\" class=\"img-rounded img-responsive\" />\n        </div>\n        <div class=\"col-xs-9 col-md-9 section-box\">\n\t        <h2>\n\t          {{item.id}} - {{item.name}} <a href=\"http://bootsnipp.com/\" target=\"_blank\"><span class=\"glyphicon glyphicon-new-window\"></span>\n\t          </a>\n\t        </h2>\n\t      \t<p>\n\t          Design elements, playground and code snippets for Bootstrap HTML/CSS/JS framework\n\t        </p>\n\t        <hr />\n\t        <div class=\"row rating-desc\">\n\t          <div class=\"col-md-12\">\n\t                                <span class=\"glyphicon glyphicon-heart\"></span><span class=\"glyphicon glyphicon-heart\">\n\t                                </span><span class=\"glyphicon glyphicon-heart\"></span><span class=\"glyphicon glyphicon-heart\">\n\t                                </span><span class=\"glyphicon glyphicon-heart\"></span>(36)<span class=\"separator\">|</span>\n\t                                <span class=\"glyphicon glyphicon-comment\"></span>(100 Comments)\n\t          </div>\n\t        </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/components/segurolist/item/item.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/components/segurolist/item/item.component.ts ***!
  \**************************************************************/
/*! exports provided: ItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemComponent", function() { return ItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ItemComponent = /** @class */ (function () {
    function ItemComponent() {
    }
    ItemComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ItemComponent.prototype, "item", void 0);
    ItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-item',
            template: __webpack_require__(/*! ./item.component.html */ "./src/app/components/segurolist/item/item.component.html"),
            styles: [__webpack_require__(/*! ./item.component.css */ "./src/app/components/segurolist/item/item.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ItemComponent);
    return ItemComponent;
}());



/***/ }),

/***/ "./src/app/components/segurolist/segurolist.component.css":
/*!****************************************************************!*\
  !*** ./src/app/components/segurolist/segurolist.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\r\n\tmargin: 0;\r\n\tpadding: 0;\r\n\tbox-sizing: border-box;\r\n}\r\n\r\nbody {\r\n\tbackground: #FAFAFA;\r\n\tfont-family: arial, helvetica, sans-serif;\r\n\tfont-size: 16px;\r\n}\r\n\r\n.wrap {\r\n\tmargin: auto;\r\n\tmax-width: 800px;\r\n\twidth: 90%;\r\n}\r\n\r\n.principal {\r\n\tbackground: #F44336;\r\n\tborder-top: 20px solid #D32F2F;\r\n\tcolor: #fff;\r\n\tpadding: 50px 0;\r\n\twidth: 100%;\r\n}\r\n\r\n.principal .formulario {\r\n\tcolor: #212121;\r\n\ttext-align: center;\r\n}\r\n\r\n.principal .formulario input[type=text] {\r\n\tmargin-bottom: 20px;\r\n\tpadding: 10px;\r\n\twidth: 100%;\r\n}\r\n\r\n.principal .formulario input[type=text].error {\r\n\tborder: 5px solid #D32F2F;\r\n\tcolor: red;\r\n}\r\n\r\n.principal .formulario .boton {\r\n\tbackground: none;\r\n\tborder: 1px solid #D32F2F;\r\n\tcolor: #fff;\r\n\tdisplay: inline-block;\r\n\tfont-size: 16px;\r\n\tpadding: 10px 15px;\r\n}\r\n\r\n/*\r\n.principal .formulario .boton:hover {\r\n\tborder: 1px solid #fff;\r\n}\r\n*/\r\n\r\n/* - Tareas - */\r\n\r\n.tareas .lista {\r\n\tlist-style: none;\r\n\tmargin-top: 12px;\r\n}\r\n\r\n/*\r\n.tareas .lista li {\r\n\tborder-bottom: 1px solid #B6B6B6;\r\n}\r\n\r\n.tareas .lista li a {\r\n\tcolor: #212121;\r\n\tdisplay: block;\r\n\tpadding: 20px 0;\r\n\ttext-decoration: none;\r\n}\r\n*/\r\n\r\n/*\r\n.tareas .lista li a:hover {\r\n\ttext-decoration: line-through;\r\n}\r\n*/\r\n\r\n/*\r\n.principal .formulario .boton:hover {\r\n  border: 1px solid #fff;\r\n}\r\n*/\r\n\r\n#center-element{\r\n  text-align: center !important;\r\n  display: flex;\r\n  align-items:flex-start;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/components/segurolist/segurolist.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/segurolist/segurolist.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"principal\">\n\t<div class=\"wrap\">\n\t\t<form class=\"formulario\" action=\"\">\n\t\t\t<div id=\"center-element\">\n              <input type=\"text\" id=\"placa\" placeholder=\"Placa del vehículo ejem abc123\" name='placa' [(ngModel)]=\"placa\">\n              <input type=\"button\" class=\"boton\" id=\"btn-buscar\" value=\"Buscar\">\n            </div>\n\t\t\t\n\t\t\t<p><a routerLink=\"/register\">no tengo placa todavia</a></p>\n\t\t</form>\n\t</div>\n</div>\n<div class=\"tareas\">\n\t<div class=\"wrap\">\n\t\t<ul class=\"lista\" id=\"lista\">\n\t\t\t<app-item *ngFor=\"let item of data | search : placa\" [item]=\"item\"></app-item>\n\t\t</ul>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/components/segurolist/segurolist.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/segurolist/segurolist.component.ts ***!
  \***************************************************************/
/*! exports provided: SegurolistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegurolistComponent", function() { return SegurolistComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SegurolistComponent = /** @class */ (function () {
    function SegurolistComponent() {
        this.lista = false;
        this.placa = '';
        this.data = [
            {
                id: 1,
                name: "client A seguros ABC",
                industry: "Industry 1"
            },
            {
                id: 2,
                name: "client Ab seguro RG",
                industry: "Industry 2"
            },
            {
                id: 3,
                name: "client megha seguro vehicular",
                industry: "Industry 5"
            },
            {
                id: 4,
                name: "shubham",
                industry: "Industry 1"
            },
            {
                id: 4,
                name: "rick",
                industry: "Industry 1"
            }
        ];
    }
    SegurolistComponent.prototype.ngOnInit = function () {
    };
    SegurolistComponent.prototype.mostarLista = function () {
        //this.lista = true;
        alert('ejemplo:' + this.placa);
    };
    SegurolistComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-segurolist',
            template: __webpack_require__(/*! ./segurolist.component.html */ "./src/app/components/segurolist/segurolist.component.html"),
            styles: [__webpack_require__(/*! ./segurolist.component.css */ "./src/app/components/segurolist/segurolist.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SegurolistComponent);
    return SegurolistComponent;
}());



/***/ }),

/***/ "./src/app/pipes/search.pipe.ts":
/*!**************************************!*\
  !*** ./src/app/pipes/search.pipe.ts ***!
  \**************************************/
/*! exports provided: SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipe", function() { return SearchPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SearchPipe = /** @class */ (function () {
    function SearchPipe() {
    }
    SearchPipe.prototype.transform = function (value, term) {
        return value.filter(function (x) { return x.name.toLowerCase().startsWith(term.toLowerCase()) || x.industry.toLowerCase().startsWith(term.toLowerCase()); });
    };
    SearchPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'search'
        })
    ], SearchPipe);
    return SearchPipe;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\edwight\Documents\Angular\daniel\api-restful\src\client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map