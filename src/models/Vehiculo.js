const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var Vehiculo = new Schema({
	clase:{type:String, enum:['motos','Vehiculo fami','accesories']},
	cilindraje:{type:String},
	antigüedad:{type:String},
	picture:{type:String},
	price:{type:Number, default:0},
	category:{type:String, enum:['computers','phone','accesories']},
	description:{type:String},
	referencia:{type:String}
},{
	collection:'items'
});


module.exports = mongoose.model('Seguro', Seguro);