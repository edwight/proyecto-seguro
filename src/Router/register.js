const express = require('express');
const RegisterController = require('../Controllers/register');
//const userCtrl = require('../Controllers/user');
const api = express.Router();
//const auth = require('../Middlewares/user');

//product index
//api.get('/product', productController.getProducts);
//product show
//api.get('/product/:productId', productController.getProduct);
//product /store
api.post('/register', registerController.saveRegister);
//product /update
api.put('/product/:productId', registerController.updateRegister);
//product /delete
//api.delete('/product/:productId', productController.deleteProduct);

//api.post('/signup', userCtrl.signUp);
//api.post('/signIn', userCtrl.singIn);

//api.get('/private',auth,(err,res)=>{
//	res.status(200).send({msg:'tienes acceso'})
//});
module.exports = api;